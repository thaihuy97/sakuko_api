﻿using SakukoForwarder.Repository;

namespace SakukoForwarder.Utilities
{
    public class Utility : IUtilityService
    {
        public IConfiguration _configuration;

        public Utility(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public string MediaType => "text/xml";


        public string GetPartnerUsernameStage => _configuration["SakukoPartner:stage:Username"] + "";

        public string GetPartnerPasswordStage => _configuration["SakukoPartner:stage:Password"] + "";

        public string GetEndpointStage => _configuration["SakukoPartner:stage:Endpoint"] + "";

        public string GetSMLInventoryUrlStage => _configuration["SakukoPartner:stage:SMLInventoryUrl"] + "";


        public string GetPartnerUsernameProd => _configuration["SakukoPartner:prod:Username"] + "";

        public string GetPartnerPasswordProd => _configuration["SakukoPartner:prod:Password"] + "";

        public string EndpointProd => _configuration["SakukoPartner:prod:Endpoint"] + "";

        public string SMLInventoryUrlProd => _configuration["SakukoPartner:prod:SMLInventoryUrl"] + "";

    }

}