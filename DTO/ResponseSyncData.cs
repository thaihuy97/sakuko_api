
namespace SakukoForwarder.DTO
{
    public class ResponseSyncData

    {
        public int Code { get; set; }
        public string Idx { get; set; }
        public string Message { get; set; }

        public ResponseSyncData(int code, string idx, string message) => (Code, Idx, Message) = (code, idx, message);
    }
    
    public class ResponseSMLInventory
    {
        public string warehouseCode { get; set; }
        public string upccode { get; set; }
        public decimal qty { get; set; }
        public ResponseSMLInventory(string warehouseCodeName, string upccodeName, decimal qtyName) {
            warehouseCode = warehouseCodeName;
            upccode = upccodeName;
            qty = qtyName;
        }
    }
}