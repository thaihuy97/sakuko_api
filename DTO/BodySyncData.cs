using SMLInventoryServiceReference;
namespace SakukoForwarder.DTO
{
    public class BodySyncData

    {
        public int docType { get; set; }
        public string docNo { get; set; }
        public string locationCode { get; set; }
        public DateTime postingDate { get; set; }
        public string itemNo { get; set; }
        public string uOM { get; set; }
        public decimal quantity { get; set; }
        public string toLocation { get; set; }
        public int transType { get; set; }
        public string statusCode { get; set; }

        public int lineNo { get; set; }

        public string userID { get; set; }

    }
    public class BodyInventoryFilter
    {
        public SMLInventory_Fields Field { get; set; }
        public string Criteria { get; set; }
    }

    public class BodyInventory

    {
        public BodyInventoryFilter[] filters { get; set; }
    }

}