﻿
namespace ServiceReference
{


    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.1.0")]
    [System.ServiceModel.ServiceContractAttribute(Namespace = "urn:microsoft-dynamics-schemas/codeunit/SMLDataSync", ConfigurationName = "ServiceReference.SMLDataSync_Port")]
    public interface SMLDataSync_Port
    {

        [System.ServiceModel.OperationContractAttribute(Action = "urn:microsoft-dynamics-schemas/codeunit/SMLDataSync:SyncSMLData", ReplyAction = "*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults = true)]
        System.Threading.Tasks.Task<ServiceReference.SyncSMLData_Result> SyncSMLDataAsync(ServiceReference.SyncSMLData request);
    }

    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.1.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(WrapperName = "SyncSMLData", WrapperNamespace = "urn:microsoft-dynamics-schemas/codeunit/SMLDataSync", IsWrapped = true)]
    public partial class SyncSMLData
    {

        [System.ServiceModel.MessageBodyMemberAttribute(Namespace = "urn:microsoft-dynamics-schemas/codeunit/SMLDataSync", Order = 0)]
        public int docType;

        [System.ServiceModel.MessageBodyMemberAttribute(Namespace = "urn:microsoft-dynamics-schemas/codeunit/SMLDataSync", Order = 1)]
        public string docNo;

        [System.ServiceModel.MessageBodyMemberAttribute(Namespace = "urn:microsoft-dynamics-schemas/codeunit/SMLDataSync", Order = 2)]
        public string locationCode;

        [System.ServiceModel.MessageBodyMemberAttribute(Namespace = "urn:microsoft-dynamics-schemas/codeunit/SMLDataSync", Order = 3)]
        [System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        public System.DateTime postingDate;

        [System.ServiceModel.MessageBodyMemberAttribute(Namespace = "urn:microsoft-dynamics-schemas/codeunit/SMLDataSync", Order = 4)]
        public string itemNo;

        [System.ServiceModel.MessageBodyMemberAttribute(Namespace = "urn:microsoft-dynamics-schemas/codeunit/SMLDataSync", Order = 5)]
        public string uOM;

        [System.ServiceModel.MessageBodyMemberAttribute(Namespace = "urn:microsoft-dynamics-schemas/codeunit/SMLDataSync", Order = 6)]
        public decimal quantity;

        [System.ServiceModel.MessageBodyMemberAttribute(Namespace = "urn:microsoft-dynamics-schemas/codeunit/SMLDataSync", Order = 7)]
        public string toLocation;

        [System.ServiceModel.MessageBodyMemberAttribute(Namespace = "urn:microsoft-dynamics-schemas/codeunit/SMLDataSync", Order = 8)]
        public int transType;

        [System.ServiceModel.MessageBodyMemberAttribute(Namespace = "urn:microsoft-dynamics-schemas/codeunit/SMLDataSync", Order = 9)]
        public string statusCode;

        [System.ServiceModel.MessageBodyMemberAttribute(Namespace = "urn:microsoft-dynamics-schemas/codeunit/SMLDataSync", Order = 9)]
        public int lineNo;

        [System.ServiceModel.MessageBodyMemberAttribute(Namespace = "urn:microsoft-dynamics-schemas/codeunit/SMLDataSync", Order = 9)]
        public string userID;

        public SyncSMLData()
        {
        }

        public SyncSMLData(int docType, string docNo, string locationCode, System.DateTime postingDate, string itemNo, string uOM, decimal quantity, string toLocation, int transType, string statusCode, int lineNo, string userID)
        {
            this.docType = docType;
            this.docNo = docNo;
            this.locationCode = locationCode;
            this.postingDate = postingDate;
            this.itemNo = itemNo;
            this.uOM = uOM;
            this.quantity = quantity;
            this.toLocation = toLocation;
            this.transType = transType;
            this.statusCode = statusCode;
            this.lineNo = lineNo;
            this.userID = userID;
        }
    }

    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.1.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(WrapperName = "SyncSMLData_Result", WrapperNamespace = "urn:microsoft-dynamics-schemas/codeunit/SMLDataSync", IsWrapped = true)]
    public partial class SyncSMLData_Result
    {

        [System.ServiceModel.MessageBodyMemberAttribute(Namespace = "urn:microsoft-dynamics-schemas/codeunit/SMLDataSync", Order = 0)]
        public string return_value;

        public SyncSMLData_Result()
        {
        }

        public SyncSMLData_Result(string return_value)
        {
            this.return_value = return_value;
        }
    }

    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.1.0")]
    public interface SMLDataSync_PortChannel : ServiceReference.SMLDataSync_Port, System.ServiceModel.IClientChannel
    {
    }

    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.1.0")]
    public partial class SMLDataSync_PortClient : System.ServiceModel.ClientBase<ServiceReference.SMLDataSync_Port>, ServiceReference.SMLDataSync_Port
    {

        /// <summary>
        /// Implement this partial method to configure the service endpoint.
        /// </summary>
        /// <param name="serviceEndpoint">The endpoint to configure</param>
        /// <param name="clientCredentials">The client credentials</param>
        static partial void ConfigureEndpoint(System.ServiceModel.Description.ServiceEndpoint serviceEndpoint, System.ServiceModel.Description.ClientCredentials clientCredentials);

        public SMLDataSync_PortClient() :
                base(SMLDataSync_PortClient.GetDefaultBinding(), SMLDataSync_PortClient.GetDefaultEndpointAddress())
        {
            this.Endpoint.Name = EndpointConfiguration.SMLDataSync_Port.ToString();
            ConfigureEndpoint(this.Endpoint, this.ClientCredentials);
        }

        public SMLDataSync_PortClient(EndpointConfiguration endpointConfiguration) :
                base(SMLDataSync_PortClient.GetBindingForEndpoint(endpointConfiguration), SMLDataSync_PortClient.GetEndpointAddress(endpointConfiguration))
        {
            this.Endpoint.Name = endpointConfiguration.ToString();
            ConfigureEndpoint(this.Endpoint, this.ClientCredentials);
        }

        public SMLDataSync_PortClient(EndpointConfiguration endpointConfiguration, string remoteAddress) :
                base(SMLDataSync_PortClient.GetBindingForEndpoint(endpointConfiguration), new System.ServiceModel.EndpointAddress(remoteAddress))
        {
            this.Endpoint.Name = endpointConfiguration.ToString();
            ConfigureEndpoint(this.Endpoint, this.ClientCredentials);
        }

        public SMLDataSync_PortClient(EndpointConfiguration endpointConfiguration, System.ServiceModel.EndpointAddress remoteAddress) :
                base(SMLDataSync_PortClient.GetBindingForEndpoint(endpointConfiguration), remoteAddress)
        {
            this.Endpoint.Name = endpointConfiguration.ToString();
            ConfigureEndpoint(this.Endpoint, this.ClientCredentials);
        }

        public SMLDataSync_PortClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) :
                base(binding, remoteAddress)
        {
        }

        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<ServiceReference.SyncSMLData_Result> ServiceReference.SMLDataSync_Port.SyncSMLDataAsync(ServiceReference.SyncSMLData request)
        {
            return base.Channel.SyncSMLDataAsync(request);
        }

        public System.Threading.Tasks.Task<ServiceReference.SyncSMLData_Result> SyncSMLDataAsync(int docType, string docNo, string locationCode, System.DateTime postingDate, string itemNo, string uOM, decimal quantity, string toLocation, int transType, string statusCode, int lineNo, string userID)
        {
            ServiceReference.SyncSMLData inValue = new ServiceReference.SyncSMLData();
            inValue.docType = docType;
            inValue.docNo = docNo;
            inValue.locationCode = locationCode;
            inValue.postingDate = postingDate;
            inValue.itemNo = itemNo;
            inValue.uOM = uOM;
            inValue.quantity = quantity;
            inValue.toLocation = toLocation;
            inValue.transType = transType;
            inValue.statusCode = statusCode;
            inValue.lineNo = lineNo;
            inValue.userID = userID;
            return ((ServiceReference.SMLDataSync_Port)(this)).SyncSMLDataAsync(inValue);
        }

        public virtual System.Threading.Tasks.Task OpenAsync()
        {
            return System.Threading.Tasks.Task.Factory.FromAsync(((System.ServiceModel.ICommunicationObject)(this)).BeginOpen(null, null), new System.Action<System.IAsyncResult>(((System.ServiceModel.ICommunicationObject)(this)).EndOpen));
        }

        private static System.ServiceModel.Channels.Binding GetBindingForEndpoint(EndpointConfiguration endpointConfiguration)
        {
            if ((endpointConfiguration == EndpointConfiguration.SMLDataSync_Port))
            {
                System.ServiceModel.BasicHttpBinding result = new System.ServiceModel.BasicHttpBinding();
                result.MaxBufferSize = int.MaxValue;
                result.ReaderQuotas = System.Xml.XmlDictionaryReaderQuotas.Max;
                result.MaxReceivedMessageSize = int.MaxValue;
                result.AllowCookies = true;
                return result;
            }
            throw new System.InvalidOperationException(string.Format("Could not find endpoint with name \'{0}\'.", endpointConfiguration));
        }

        private static System.ServiceModel.EndpointAddress GetEndpointAddress(EndpointConfiguration endpointConfiguration)
        {
            if ((endpointConfiguration == EndpointConfiguration.SMLDataSync_Port))
            {
                return new System.ServiceModel.EndpointAddress("http://navtest.sakuko.vn:7047/NAV_TEST_01/WS/FC_VPH_01/Codeunit/SMLDataSync");
            }
            throw new System.InvalidOperationException(string.Format("Could not find endpoint with name \'{0}\'.", endpointConfiguration));
        }

        private static System.ServiceModel.Channels.Binding GetDefaultBinding()
        {
            return SMLDataSync_PortClient.GetBindingForEndpoint(EndpointConfiguration.SMLDataSync_Port);
        }

        private static System.ServiceModel.EndpointAddress GetDefaultEndpointAddress()
        {
            return SMLDataSync_PortClient.GetEndpointAddress(EndpointConfiguration.SMLDataSync_Port);
        }

        public enum EndpointConfiguration
        {

            SMLDataSync_Port,
        }
    }
}
