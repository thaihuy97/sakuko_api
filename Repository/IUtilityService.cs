﻿namespace SakukoForwarder.Repository
{
    public interface IUtilityService
    {
        string GetPartnerUsernameStage { get; }
        string GetPartnerUsernameProd { get; }
        string GetPartnerPasswordStage { get; }
        string GetPartnerPasswordProd { get; }
        string GetEndpointStage { get; }
        string EndpointProd { get; }
        string MediaType { get; }
        string GetSMLInventoryUrlStage { get; }
        string SMLInventoryUrlProd { get; }

    }

}
