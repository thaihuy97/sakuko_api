using System.Net;
using System.Text;
using System.Xml;
using SakukoForwarder.DTO;
using SakukoForwarder.Repository;
using ServiceReference;
using System.ServiceModel;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http.HttpResults;
using SMLInventoryServiceReference;

namespace SakukoForwarder.Services;

public class SakukoPartnerService
{
    private readonly IUtilityService _utility;

    public SakukoPartnerService(IUtilityService utility)
    {
        _utility = utility;
    }

    public ActionResult<Dictionary<string, string>> GetEndponitByEnv(string env)
    {
        Dictionary<string, string> endpoint = new Dictionary<string, string>();

        if (env == "PRODUCTION" || env == "CRON_PROD" || env == "QUEUE_PROD")
        {
            endpoint.Add("Endpoint", _utility.EndpointProd);
        }
        else
        {
            endpoint.Add("Endpoint", _utility.GetEndpointStage);
        }
        endpoint.Add("env", env);
        return endpoint;
    }

    public async Task<List<ResponseSyncData>> SyncPOToNAV(List<BodySyncData> bodySyncData, string env)
    {
        try
        {
            List<ResponseSyncData> responseSyncDatas = new List<ResponseSyncData>();
            BasicHttpBinding binding = new BasicHttpBinding(BasicHttpSecurityMode.TransportCredentialOnly);
            binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Windows;
            binding.MaxReceivedMessageSize = int.MaxValue;
            binding.MaxBufferPoolSize = long.MaxValue;
            binding.MaxBufferSize = int.MaxValue;

            string findUrlByEnv = "";
            string findUsername = "";
            string findPassword = "";

            if (env == "PRODUCTION" || env == "CRON_PROD" || env == "QUEUE_PROD")
            {
                findUrlByEnv = _utility.EndpointProd;
                findUsername = _utility.GetPartnerUsernameProd;
                findPassword = _utility.GetPartnerPasswordProd;
            }
            else
            {
                findUrlByEnv = _utility.GetEndpointStage;
                findUsername = _utility.GetPartnerUsernameStage;
                findPassword = _utility.GetPartnerPasswordStage;

            }

            EndpointAddress Endpoint = new EndpointAddress(findUrlByEnv);

            var client = new SMLDataSync_PortClient(binding, Endpoint);

            client.ClientCredentials.Windows.AllowedImpersonationLevel = System.Security.Principal.TokenImpersonationLevel.Identification;

            client.ClientCredentials.Windows.ClientCredential = new NetworkCredential(findUsername, findPassword);

            await client.OpenAsync();

            for (int i = 0; i < bodySyncData.Count; i++)
            {
                BodySyncData item = bodySyncData[i];
                var result = await client.SyncSMLDataAsync(item.docType, item.docNo, item.locationCode, DateTime.Now, item.itemNo, item.uOM, item.quantity, item.toLocation, item.transType, item.statusCode, item.lineNo, item.userID);
                responseSyncDatas.Add(new ResponseSyncData(String.Compare(result.return_value, "Success") == 0 ? 1 : 0, item.itemNo, result.return_value));
            }

            return responseSyncDatas;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public async Task<List<ResponseSyncData>> SyncSOToNAV(List<BodySyncData> bodySyncData, string env)
    {
        try
        {
            List<ResponseSyncData> responseSyncDatas = new List<ResponseSyncData>();
            BasicHttpBinding binding = new BasicHttpBinding(BasicHttpSecurityMode.TransportCredentialOnly);
            binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Windows;
            binding.MaxReceivedMessageSize = int.MaxValue;
            binding.MaxBufferPoolSize = long.MaxValue;
            binding.MaxBufferSize = int.MaxValue;

            string findUrlByEnv = "";
            string findUsername = "";
            string findPassword = "";

            if (env == "PRODUCTION" || env == "CRON_PROD" || env == "QUEUE_PROD")
            {
                findUrlByEnv = _utility.EndpointProd;
                findUsername = _utility.GetPartnerUsernameProd;
                findPassword = _utility.GetPartnerPasswordProd;
            }
            else
            {
                findUrlByEnv = _utility.GetEndpointStage;
                findUsername = _utility.GetPartnerUsernameStage;
                findPassword = _utility.GetPartnerPasswordStage;
            }

            EndpointAddress Endpoint = new EndpointAddress(findUrlByEnv);

            var client = new SMLDataSync_PortClient(binding, Endpoint);

            client.ClientCredentials.Windows.AllowedImpersonationLevel = System.Security.Principal.TokenImpersonationLevel.Identification;

            client.ClientCredentials.Windows.ClientCredential = new NetworkCredential(findUsername, findPassword);

            await client.OpenAsync();

            for (int i = 0; i < bodySyncData.Count; i++)
            {
                BodySyncData item = bodySyncData[i];
                var result = await client.SyncSMLDataAsync(item.docType, item.docNo, item.locationCode, DateTime.Now, item.itemNo, item.uOM, item.quantity, item.toLocation, item.transType, item.statusCode, item.lineNo, item.userID);
                responseSyncDatas.Add(new ResponseSyncData(String.Compare(result.return_value, "Success") == 0 ? 1 : 0, item.itemNo, result.return_value));
            }

            return responseSyncDatas;
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }

    public async Task<ResponseSyncData> SendRequestToNAV(BodySyncData body, string env)
    {
        try
        {
            BasicHttpBinding binding = new BasicHttpBinding(BasicHttpSecurityMode.TransportCredentialOnly);
            binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Windows;
            binding.MaxReceivedMessageSize = int.MaxValue;
            binding.MaxBufferPoolSize = long.MaxValue;
            binding.MaxBufferSize = int.MaxValue;
            string findUrlByEnv = "";
            string findUsername = "";
            string findPassword = "";
            if (env == "PRODUCTION" || env == "CRON_PROD" || env == "QUEUE_PROD")
            {
                findUrlByEnv = _utility.EndpointProd;
                findUsername = _utility.GetPartnerUsernameProd;
                findPassword = _utility.GetPartnerPasswordProd;
            }
            else
            {
                findUrlByEnv = _utility.GetEndpointStage;
                findUsername = _utility.GetPartnerUsernameStage;
                findPassword = _utility.GetPartnerPasswordStage;
            }

            // Console.WriteLine("username: " + findUsername);
            // Console.WriteLine("pass: " + findPassword);

            EndpointAddress Endpoint = new EndpointAddress(findUrlByEnv);

            var client = new SMLDataSync_PortClient(binding, Endpoint);

            client.ClientCredentials.Windows.AllowedImpersonationLevel = System.Security.Principal.TokenImpersonationLevel.Identification;

            client.ClientCredentials.Windows.ClientCredential = new NetworkCredential(findUsername, findPassword);

            await client.OpenAsync();

            var result = await client.SyncSMLDataAsync(body.docType, body.docNo, body.locationCode, DateTime.Now, body.itemNo, body.uOM, body.quantity, body.toLocation, body.transType, body.statusCode, body.lineNo, body.userID);
            ResponseSyncData responseSyncData = new ResponseSyncData(String.Compare(result.return_value, "Success") == 0 ? 1 : 0, body.itemNo, result.return_value);
            return responseSyncData;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public async Task<SMLInventory[]> GetSMLInventoryFromNAV(BodyInventory body, string env)
    {
        try
        {
            BasicHttpBinding binding = new BasicHttpBinding(BasicHttpSecurityMode.TransportCredentialOnly);
            binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Windows;
            binding.MaxReceivedMessageSize = int.MaxValue;
            binding.MaxBufferPoolSize = long.MaxValue;
            binding.MaxBufferSize = int.MaxValue;
            string findUrlByEnv = "";
            string findUsername = "";
            string findPassword = "";
            if (env == "PRODUCTION" || env == "CRON_PROD" || env == "QUEUE_PROD")
            {
                findUrlByEnv = _utility.SMLInventoryUrlProd;
                findUsername = _utility.GetPartnerUsernameProd;
                findPassword = _utility.GetPartnerPasswordProd;
            }
            else
            {
                findUrlByEnv = _utility.GetSMLInventoryUrlStage;
                findUsername = _utility.GetPartnerUsernameStage;
                findPassword = _utility.GetPartnerPasswordStage;
            }

            EndpointAddress Endpoint = new EndpointAddress(findUrlByEnv);

            var client = new SMLInventory_PortClient(binding, Endpoint);

            client.ClientCredentials.Windows.AllowedImpersonationLevel = System.Security.Principal.TokenImpersonationLevel.Identification;

            client.ClientCredentials.Windows.ClientCredential = new NetworkCredential(findUsername, findPassword);

            await client.OpenAsync();

            List<SMLInventory_Filter> filtersList = new List<SMLInventory_Filter>();
            foreach (BodyInventoryFilter filter in body.filters)
            {
                SMLInventory_Filter f = new SMLInventory_Filter();
                f.Field = filter.Field;
                f.Criteria = filter.Criteria;
                filtersList.Add(f);
            }

            ReadMultiple_Result result = await client.ReadMultipleAsync(filtersList.ToArray(), "", 999999);
            SMLInventory[] responseSyncData = result.ReadMultiple_Result1.Where(f => f.Inventory != 0).ToArray();
            return responseSyncData;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

}