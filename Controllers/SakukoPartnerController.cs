using System.Xml;
using Microsoft.AspNetCore.Mvc;
using SakukoForwarder.Services;
using System.Net;
using System.Text;
using SakukoForwarder.DTO;

namespace SakukoForwarder.Controllers;

[ApiController]
[Route("api/[controller]")]
public class SakukoPartnerController : ControllerBase
{
    private readonly ILogger<SakukoPartnerController> _logger;
    private readonly SakukoPartnerService _sakukoPartnerService;

    // private readonly IUtilityService _utility;

    public SakukoPartnerController(
        ILogger<SakukoPartnerController> logger,
        SakukoPartnerService sakukoPartnerService
        )
    {
        _logger = logger;
        _sakukoPartnerService = sakukoPartnerService;
    }

    [HttpGet]
    [Route("get")]
    public IActionResult GetUsername()
    {
        Console.WriteLine("****** Start App Sakuko *****");
        return Ok();
    }

    [HttpGet]
    [Route("test")]
    public IActionResult Test()
    {
        return Ok("cc");
    }

    [HttpGet("GetAllHeaders")]
    public ActionResult<Dictionary<string, string>> GetAllHeaders()
    {
        Dictionary<string, string> requestHeaders = new Dictionary<string, string>();
        foreach (var header in Request.Headers)
        {
            requestHeaders.Add(header.Key, header.Value);
        }
        return requestHeaders;
    }

    [HttpGet("GetEndponitByEnv")]
    public ActionResult<Dictionary<string, string>> GetEndponitByEnv()
    {
        Dictionary<string, string> requestHeaders = new Dictionary<string, string>();
        foreach (var header in Request.Headers)
        {
            requestHeaders.Add(header.Key, header.Value);
        }
        return _sakukoPartnerService.GetEndponitByEnv(requestHeaders["env"]);
    }

    [HttpPost]
    [Route("SyncPOToNAV")]
    public async Task<List<ResponseSyncData>> SyncPOToNAV(List<BodySyncData> bodySyncData)
    {
        Dictionary<string, string> requestHeaders = new Dictionary<string, string>();
        foreach (var header in Request.Headers)
        {
            requestHeaders.Add(header.Key, header.Value);
        }
        return await _sakukoPartnerService.SyncPOToNAV(bodySyncData, requestHeaders["env"]);
    }

    [HttpPost]
    [Route("SyncSOToNAV")]
    public async Task<List<ResponseSyncData>> SyncSOToNAV(List<BodySyncData> bodySyncData)
    {
        Dictionary<string, string> requestHeaders = new Dictionary<string, string>();
        foreach (var header in Request.Headers)
        {
            requestHeaders.Add(header.Key, header.Value);
        }
        return await _sakukoPartnerService.SyncSOToNAV(bodySyncData, requestHeaders["env"]);
    }


    [HttpPost]
    [Route("SendRequestToNAV")]
    public async Task<ResponseSyncData> SendRequestToNAV(BodySyncData reqBody)
    {
        Dictionary<string, string> requestHeaders = new Dictionary<string, string>();
        foreach (var header in Request.Headers)
        {
            requestHeaders.Add(header.Key, header.Value);
        }
        return await _sakukoPartnerService.SendRequestToNAV(reqBody, requestHeaders["env"]);
    }

    [HttpPost]
    [Route("GetSMLInventory")]
    public async Task<ResponseSMLInventory[]> GetSMLInventory(BodyInventory reqBody)
    {
        Dictionary<string, string> requestHeaders = new Dictionary<string, string>();
        foreach (var header in Request.Headers)
        {
            requestHeaders.Add(header.Key, header.Value);
        }
        var results = await _sakukoPartnerService.GetSMLInventoryFromNAV(reqBody, requestHeaders["env"]);
        var res = new List<ResponseSMLInventory>();
        foreach (var item in results)
        {
            res.Add(new ResponseSMLInventory(item.Location_Code, item.Item_No, item.Inventory));
        }
        return res.ToArray();
    }

}
